module gitlab.com/crypdroid.io/libs/excelsior

go 1.12

require (
	github.com/go-kit/kit v0.9.0
	github.com/pkg/errors v0.8.1
	github.com/spf13/viper v1.4.0
	github.com/stretchr/testify v1.3.0
	github.com/theMattCode/go-binance v1.0.0
)
