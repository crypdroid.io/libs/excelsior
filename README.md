# Excelsior - Crypdroid's library for working with crypto currencies, markets and exchanges

![pipeline status](https://gitlab.com/crypdroid.io/libs/excelsior/badges/master/pipeline.svg)
![coverage report](https://gitlab.com/crypdroid.io/libs/excelsior/badges/master/coverage.svg)
