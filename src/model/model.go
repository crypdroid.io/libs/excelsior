package model

const (
	Satoshi = 0.00000001

	Minute         = Interval(60)
	ThreeMinutes   = Interval(3 * Minute)
	FiveMinutes    = Interval(5 * Minute)
	FifteenMinutes = Interval(15 * Minute)
	ThirtyMinutes  = Interval(30 * Minute)
	Hour           = Interval(60 * Minute)
	TwoHours       = Interval(2 * Hour)
	FourHours      = Interval(4 * Hour)
	SixHours       = Interval(6 * Hour)
	EightHours     = Interval(8 * Hour)
	TwelveHours    = Interval(12 * Hour)
	Day            = Interval(24 * Hour)
)

// Interval is a named duration of time measured in seconds.
type Interval int64

type Pair struct {
	Symbol         string
	Status         string
	Quote          string
	QuotePrecision int
	Base           string
	BasePrecision  int
}
