package exchange

import (
	"github.com/spf13/viper"
	"gitlab.com/crypdroid.io/libs/excelsior/src/model"
)

type Adapter interface {
	Name() string
	Ping() error
	Pairs() ([]*model.Pair, error)
}

type AdapterFactory func(config *viper.Viper) (Adapter, error)

var adapterFactories map[string]AdapterFactory

func GetAdapterFactory(name string) AdapterFactory {
	if adapterFactories == nil {
		adapterFactories = initAdapterFactories()
	}
	return adapterFactories[name]
}

func initAdapterFactories() map[string]AdapterFactory {
	return map[string]AdapterFactory{
		Binance: CreateBinanceAdapter,
	}
}
