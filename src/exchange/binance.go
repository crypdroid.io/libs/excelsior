package exchange

import (
	"context"
	"github.com/go-kit/kit/log"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"github.com/theMattCode/go-binance"
	"gitlab.com/crypdroid.io/libs/excelsior/src/model"
	"os"
)

const (
	Binance             = "Binance"
	BinanceUrlConfigKey = "exchange.binance.url"
	BinanceApiKey       = "EXCHANGE_BINANCE_APIKEY"
	BinanceApiSecret    = "EXCHANGE_BINANCE_APISECRET"
)

type binanceAdapter struct {
	service            binance.Service
	pairs              []*model.Pair
	cachedExchangeInfo *binance.ExchangeInfo
}

func (b binanceAdapter) Name() string {
	return Binance
}

func (b *binanceAdapter) Ping() error {
	return b.service.Ping()
}

func (b *binanceAdapter) Pairs() ([]*model.Pair, error) {
	if b.pairs == nil {
		if b.cachedExchangeInfo == nil {
			exchangeInfo, err := b.service.ExchangeInfo()
			if err != nil {
				return nil, err
			}
			b.cachedExchangeInfo = exchangeInfo
			symbols := exchangeInfo.Symbols
			pairs := make([]*model.Pair, len(symbols))
			for i, symbol := range symbols {
				pairs[i] = &model.Pair{
					Symbol:         symbol.Symbol,
					Status:         symbol.Status,
					Base:           symbol.BaseAsset,
					BasePrecision:  symbol.BaseAssetPrecision,
					Quote:          symbol.QuoteAsset,
					QuotePrecision: symbol.QuotePrecision,
				}
			}
			b.pairs = pairs
		}
	}
	return b.pairs, nil
}

func CreateBinanceAdapter(config *viper.Viper) (Adapter, error) {
	apiKey := config.GetString(BinanceApiKey)
	if apiKey == "" {
		return nil, errors.New(BinanceApiKey + " not configured")
	}

	apiSecret := config.GetString(BinanceApiSecret)
	if apiSecret == "" {
		return nil, errors.New(BinanceApiSecret + " not configured")
	}

	url := config.GetString(BinanceUrlConfigKey)
	if url == "" {
		return nil, errors.New(BinanceUrlConfigKey + " not configured")
	}

	hmacSigner := &binance.HmacSigner{Key: []byte(apiKey)}
	ctx, _ := context.WithCancel(context.Background())
	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	logger = log.With(logger, "time", log.DefaultTimestampUTC, "caller", log.DefaultCaller)
	service := binance.NewAPIService(url, apiSecret, hmacSigner, logger, ctx)
	return &binanceAdapter{service: service}, nil
}
