package exchange_test

import (
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	. "gitlab.com/crypdroid.io/libs/excelsior/src/exchange"
	"os"
	"testing"
)

func setEnv(key, value string, t *testing.T) {
	err := os.Setenv(key, value)
	if err != nil {
		t.Errorf("set %s failed: %s", key, err)
	}
}

func TestBinanceAdapterFactory_Create(t *testing.T) {
	setEnv(BinanceApiKey, "key", t)
	defer setEnv(BinanceApiKey, "", t)
	setEnv(BinanceApiSecret, "secret", t)
	defer setEnv(BinanceApiSecret, "", t)

	config := viper.New()
	config.AutomaticEnv()
	config.Set(BinanceUrlConfigKey, "https://www.binance.com")

	exchange, e := CreateBinanceAdapter(config)
	assert.NoErrorf(t, e, "could not create exchange: %s", e)
	assert.Equalf(t, Binance, exchange.Name(), "exchange name should be %s", Binance)
}

func TestBinanceAdapterFactory_Create_ApiKeyMissing(t *testing.T) {
	config := viper.New()
	testBinanceAdapterFactoryCreateErrors(config, t, BinanceApiKey)
}

func TestBinanceAdapterFactory_Create_ApiSecretMissing(t *testing.T) {
	setEnv(BinanceApiKey, "key", t)
	defer setEnv(BinanceApiKey, "", t)
	config := viper.New()
	config.AutomaticEnv()
	testBinanceAdapterFactoryCreateErrors(config, t, BinanceApiSecret)
}

func TestBinanceAdapterFactory_Create_UrlMissing(t *testing.T) {
	setEnv(BinanceApiKey, "key", t)
	defer setEnv(BinanceApiKey, "", t)
	setEnv(BinanceApiSecret, "secret", t)
	defer setEnv(BinanceApiSecret, "", t)
	config := viper.New()
	config.AutomaticEnv()
	testBinanceAdapterFactoryCreateErrors(config, t, BinanceUrlConfigKey)
}

func testBinanceAdapterFactoryCreateErrors(config *viper.Viper, t *testing.T, contains string) {
	_, err := CreateBinanceAdapter(config)
	assert.Error(t, err)
	assert.Contains(t, err.Error(), contains)
}

func binanceAdapterForIntegrationTest(t *testing.T) Adapter {
	config := viper.New()
	config.SetEnvPrefix("INTEGRATION_TEST")
	config.AutomaticEnv()
	config.Set(BinanceUrlConfigKey, "https://www.binance.com")
	factory := GetAdapterFactory(Binance)
	adapter, err := factory(config)
	assert.NoError(t, err)
	assert.NotNil(t, adapter)
	return adapter
}

func TestBinanceAdapter_Ping(t *testing.T) {
	adapter := binanceAdapterForIntegrationTest(t)
	err := adapter.Ping()
	assert.NoError(t, err)
}

func TestBinanceAdapter_Pairs(t *testing.T) {
	adapter := binanceAdapterForIntegrationTest(t)
	pairs, err := adapter.Pairs()
	assert.NoError(t, err)
	assert.NotNil(t, pairs)
	assert.NotZero(t, len(pairs))
}
